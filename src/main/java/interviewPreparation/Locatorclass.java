package interviewPreparation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Locatorclass {
	
	@FindBy(xpath="//select")
    private WebElement selectTagMenu;
	@FindBy(xpath="//select/option[text()='Audi']")
	private WebElement selectTagAudiSubMenu;
	@FindBy(className="dropbtn")
	private WebElement clickableMenu;
	@FindBy(xpath="//ul[@id='myDropdown']/li/a[text()='Google']")
	private WebElement clickableSubMenu;
	@FindBy(className="dropdownHover")
	private WebElement hoverableMenu;
	@FindBy(xpath="//div[@class='dropdown-contentHover']/a[1]")
	private WebElement hoverableSubMenu;
	
	public WebElement getSelectTagMenu() {
		return selectTagMenu;
	}
	public WebElement getSelectTagAudiSubMenu() {
		return selectTagAudiSubMenu;
	}
	public WebElement getClickableMenu() {
		return clickableMenu;
	}
	public WebElement getClickableSubMenu() {
		return clickableSubMenu;
	}
	public WebElement getHoverableMenu() {
		return hoverableMenu;
	}
	public WebElement getHoverableSubMenu() {
		return hoverableSubMenu;
	}
	public static Locatorclass getLocatorclass(WebDriver driver) {
		return PageFactory.initElements(driver, Locatorclass.class);
		
	}
}
