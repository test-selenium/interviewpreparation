package MouseOveroperations;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class moveToElement {
	WebDriver driver;
    @Test
    public void tc_01() throws InterruptedException {
 	   System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\91889\\Desktop\\Freecrmframework\\freecrmapplication\\Drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.spicejet.com/");
		WebElement ele=driver.findElement(By.id("highlight-addons"));
		Actions act=new Actions(driver);
		act.moveToElement(ele);
		Thread.sleep(3000);
		WebElement subMenu=driver.findElement(By.xpath("//a[contains(text(),'SpiceMax')]"));
		act.moveToElement(subMenu);
		act.click().build().perform();
    }

}
//highlight-addons