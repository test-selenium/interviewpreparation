package MouseOveroperations;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class MouseOverelement {
	WebDriver driver;
       @Test
       public void tc_01() {
    	   System.setProperty("webdriver.chrome.driver",
   				"C:\\Users\\91889\\Desktop\\Freecrmframework\\freecrmapplication\\Drivers\\chromedriver.exe");
   		driver=new ChromeDriver();
   		driver.manage().window().maximize();
   		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
   		driver.get("https://www.apple.com");
   		WebElement ele=driver.findElement(By.xpath("//li//a[@class='ac-gn-link ac-gn-link-ipad']"));
   		Actions act=new Actions(driver);
   		act.moveToElement(ele).click().perform();
       }
}
