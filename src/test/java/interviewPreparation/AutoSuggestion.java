package interviewPreparation;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class AutoSuggestion {
	WebDriver driver;
	@Test
	public void TC_01() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\91889\\Desktop\\Freecrmframework\\freecrmapplication\\Drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.google.com");
		//driver.findElement(By.className("gLFyf gsfi")).sendKeys("india");
        driver.findElement(By.name("q")).sendKeys("selenium");
        Thread.sleep(3000);
		List<WebElement> sugeestion=driver.findElements(By.xpath("//ul[@role='listbox']//li//descendant::div[@class='sbl1']"));
		System.out.println(sugeestion.size());
		for(int i=0;i<sugeestion.size();i++) {
			String listitem=sugeestion.get(i).getText();
			System.out.println(listitem);
			if(listitem.contains("selenium tutorial")) {
				sugeestion.get(i).click();
				break;
				
			}
		}
		

}
}
