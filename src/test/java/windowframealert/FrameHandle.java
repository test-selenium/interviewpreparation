package windowframealert;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class FrameHandle {
	WebDriver driver;
	@Test
	public void TC_01() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\91889\\Desktop\\Freecrmframework\\freecrmapplication\\Drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://paytm.com/");
		WebDriverWait wait=new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='_3ac-']"))).click();
		WebElement frame=driver.findElement(By.xpath("//iframe[contains(@src,'login')]"));
		driver.switchTo().frame(frame);
		driver.findElement(By.xpath("//span[contains(text(),'Login/Signup with mobile number and password')]")).click();
		
	}

}
