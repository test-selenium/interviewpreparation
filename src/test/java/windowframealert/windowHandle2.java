package windowframealert;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class windowHandle2 {
	 WebDriver driver;
		
		@Test
		public void TC_01() throws InterruptedException {
			System.setProperty("webdriver.chrome.driver",
					"C:\\Users\\91889\\Desktop\\Freecrmframework\\freecrmapplication\\Drivers\\chromedriver.exe");
			driver=new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.get("https://www.spicejet.com/default.aspx");
			driver.findElement(By.xpath("//a[contains(text(),'Cargo')]")).click();
			Set<String> wids=driver.getWindowHandles();
		    Iterator<String> itr=	wids.iterator();
		    while(itr.hasNext()) {
		    	String p_window=itr.next();
		    	String child_wind=itr.next();
		    	driver.switchTo().window(child_wind);
		    	String title=driver.getTitle();
		    	System.out.println(title);
		    	driver.switchTo().window(p_window);
		    	String p_title=driver.getTitle();
		    	System.out.println(p_title);
		    	
		    	
		    }
		}

}
