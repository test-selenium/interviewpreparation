package windowframealert;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class WindowHandle {
       WebDriver driver;
	
	@Test
	public void TC_01() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\91889\\Desktop\\Freecrmframework\\freecrmapplication\\Drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.spicejet.com/default.aspx");
		driver.findElement(By.xpath("//a[contains(text(),'Cargo')]")).click();
		Set<String> wids=driver.getWindowHandles();
	    Iterator<String> itr=	wids.iterator();
	    String parent_wid=itr.next();
	    System.out.println(parent_wid);
	    String child_wid=itr.next();
	    System.out.println(child_wid);
	    driver.switchTo().window(child_wid);
	    String title=driver.getTitle();
	    System.out.println(title);
	    Thread.sleep(3000);
	    driver.findElement(By.xpath("//div[@class='innertext']//p[contains(text(),'High Value Cargo')]")).click();
	    driver.switchTo().window(parent_wid);
	    String parent_title=driver.getTitle();
	    System.out.println(parent_title);
	    //Alert alt=driver.switchTo().alert();
	    //alt.accept();
	    
		
		//driver.findElement(By.xpath("//div[@class='//']//button[contains(text(),'Search')]")).click();
		

}
}
