package FailedTestcases;

import javax.xml.crypto.dsig.keyinfo.RetrievalMethod;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class MyRetry implements IRetryAnalyzer  {
     private int retrycount=0;
     private static final int maxretry=3;
	@Override
	public boolean retry(ITestResult result) {
		// TODO Auto-generated method stub
		if(retrycount<maxretry) {
			retrycount++;
			return true;
		}
		return false;
	}

}
