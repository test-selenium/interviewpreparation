package Dropdown;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class bootstrapdropdown {
	WebDriver driver;
	@Test
	public void TC_01() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\91889\\Desktop\\Freecrmframework\\freecrmapplication\\Drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://getbootstrap.com/docs/4.0/components/dropdowns/");
		driver.findElement(By.id("dropdownMenuButton")).click();
		List<WebElement> menu_list=driver.findElements(By.xpath("//div[@class='dropdown-menu show']//a"));
		System.out.println(menu_list.size());
		for(WebElement menu_name:menu_list) {
			String drop_values=menu_name.getText();
			System.out.println(drop_values);
			if(drop_values.equalsIgnoreCase("Action")) {
				menu_name.click();
				break;
			}
		}
	}
}
