package Dropdown;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class buttondropdown {
	WebDriver driver;
	@Test
	public void TC_01() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\91889\\Desktop\\Freecrmframework\\freecrmapplication\\Drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.kotak.com/en/calculators/emi-calculator.html");
		driver.findElement(By.xpath("//button[@class='btn btn-default dropdown-toggle']")).click();
		List<WebElement> bank_list=driver.findElements(By.xpath("//ul[@class='dropdown-menu']//li"));
		System.out.println(bank_list.size());
		for(int i=0;i<bank_list.size();i++) {
			String bank_names=bank_list.get(i).getText();
			System.out.println(bank_names);
			if(bank_names.equalsIgnoreCase("Kotak Mahindra Investments")) {
				bank_list.get(i).click();
			}
		}
	}

}
