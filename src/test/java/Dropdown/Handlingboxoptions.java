package Dropdown;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import interviewPreparation.Locatorclass;

public class Handlingboxoptions {
	WebDriver driver;
	WebElement ele;
	public static void selectMenuSubmenu(WebDriver driver, WebElement menu, WebElement submenu, String ActionType) throws InterruptedException
	{
		if(ActionType.equalsIgnoreCase("Click")) {
			menu.click();
			submenu.click();
	}else {
		Actions act=new Actions(driver);
		act.moveToElement(menu).build().perform();
		Thread.sleep(3000);
		submenu.click();
	}
	}
	@Test
	public void dropdown() throws InterruptedException {
		
	  
	 	   System.setProperty("webdriver.chrome.driver",
					"C:\\Users\\91889\\Desktop\\Freecrmframework\\freecrmapplication\\Drivers\\chromedriver.exe");
			driver=new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.get("file:///C:/Users/91889/Desktop/DDTD.html");
		
			
			WebElement selectmenu=Locatorclass.getLocatorclass(driver).getSelectTagMenu();
			WebElement selectSubmenu= Locatorclass.getLocatorclass(driver).getSelectTagAudiSubMenu();
			Handlingboxoptions.selectMenuSubmenu(driver, selectmenu, selectSubmenu, "Click");
			Thread.sleep(10000);
            
            driver.navigate().back();
			Thread.sleep(5000);
			WebElement hoverablemenu= Locatorclass.getLocatorclass(driver).getHoverableMenu();
			WebElement hoverableSubmenu= Locatorclass.getLocatorclass(driver).getHoverableSubMenu();
			Handlingboxoptions.selectMenuSubmenu(driver, hoverablemenu, hoverableSubmenu, "Hover");
			Thread.sleep(10000);
			
            WebElement clickablemenu= Locatorclass.getLocatorclass(driver).getClickableMenu();
			WebElement clickableSubmenu= Locatorclass.getLocatorclass(driver).getClickableSubMenu();
			Handlingboxoptions.selectMenuSubmenu(driver, clickablemenu, clickableSubmenu, "Click");
			//Thread.sleep(10000);
	}

}
