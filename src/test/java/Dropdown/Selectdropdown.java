package Dropdown;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Selectdropdown {
	WebDriver driver;
	@Test
	public void TC_01() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\91889\\Desktop\\Freecrmframework\\freecrmapplication\\Drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.facebook.com");
		WebElement month_dropdown=driver.findElement(By.id("month"));
		Select sel=new Select(month_dropdown);
		List<WebElement> month_list=sel.getOptions();
		int total_size=month_list.size();
		System.out.println(total_size);
	    Assert.assertEquals(total_size, 13);
	    for(WebElement month_name:month_list) {
	    	String m_name=month_name.getText();
	    	System.out.println("Month is:"+m_name);
	    	if(m_name.equalsIgnoreCase("Apr")) {
	    		month_name.click();
	    		break;
	    	}
	    	
	    }
	}

}
