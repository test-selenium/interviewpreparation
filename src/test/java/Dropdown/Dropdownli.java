package Dropdown;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Dropdownli {
	WebDriver driver;

	@Test
	public void TC_01() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\91889\\Desktop\\Freecrmframework\\freecrmapplication\\Drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.spicejet.com/default.aspx");
		driver.findElement(By.id("ctl00_mainContent_ddl_originStation1_CTXT")).click();
		List<WebElement> frm_city = driver.findElements(By.xpath("//div[@class='dropdownDiv']//ul//li"));
		System.out.println(frm_city.size());
		for (int i = 0; i < frm_city.size(); i++) {
			String city_name = frm_city.get(i).getText();
			System.out.println(city_name);
			if (city_name.equalsIgnoreCase("Delhi (DEL)")) {
				frm_city.get(i).click();
			}
		}

	}

}
