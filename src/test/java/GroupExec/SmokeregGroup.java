package GroupExec;

import org.testng.annotations.Test;

public class SmokeregGroup {
	
	@Test(groups= {"Smoke"})
	public void smoketest() {
		System.out.println("smoke test execution");
	}
	@Test(groups= {"Smoke","Regression"})
	public void smokereg() {
		System.out.println("smokereg test execution");
	}
	@Test(groups= {"Regression"})
	public void Regression() {
		System.out.println("Regression test execution");
	}
	@Test(groups= {"Regression"})
	public void Regression1() {
		System.out.println("Regression1 test execution");
	}
	@Test(groups= {"Smoke"})
	public void smoketest1() {
		System.out.println("smoke1 test execution");
	}
	
	

}
