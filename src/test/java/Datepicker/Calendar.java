package Datepicker;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Calendar {
	WebDriver driver;
	@Test
	public void TC_01() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\91889\\Desktop\\Freecrmframework\\freecrmapplication\\Drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.spicejet.com/default.aspx");
		driver.findElement(By.xpath("//input[@id='ctl00_mainContent_view_date1']")).click();
		List<WebElement> dates=driver.findElements(By.xpath("//div[@class='ui-datepicker-group ui-datepicker-group-first']//table//tr//td"));
		System.out.println(dates.size());
		for(int i=0;i<dates.size();i++) {
			String date=dates.get(i).getText();
			System.out.println(date);
			if(date.equalsIgnoreCase("8")) {
				dates.get(i).click();
			}
		}
	}

}
