package TestNGTutorial;

import org.testng.annotations.Test;

public class Prioritytestcase {
	
	@Test(priority = 1)
	public void tc_01() {
		System.out.println("Test Priority tc_01");
	}
	@Test(priority = 3)
	public void tc_02() {
		System.out.println("Test Priority tc_03");
	}
	@Test(priority = 2)
	public void tc_03() {
		System.out.println("Test Priority tc_02");
	}
	
	@Test
	public void tc_05() {
		System.out.println("No Priority");
	}
	

}
