package TestNGTutorial;

import org.testng.annotations.Test;

public class timeout {
	
	@Test
	public void tc_01() {
		System.out.println("test without timeout");
	}
	@Test(timeOut = 1000)
	public void tc_02() throws InterruptedException {
		Thread.sleep(500);
		System.out.println("test will be passed if this test will execute withing the specified time frame else failed");
	}

}
