package TestNGTutorial;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Basicannotation {

	@BeforeSuite
	public void beforeSuite() {
		System.out.println("The annotated method will run before all test in this suite have run");
	}

	@BeforeTest
	public void beforetest() {
		System.out.println("The annoted method before any test method beloging to classes inside any test tag is run");
	}

	@BeforeClass
	public void beforeclass() {
		System.out.println("The annotated method will be run before first test method in the current class is invoked");
	}

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("The annotated method will be run before each test method");

	}
	@BeforeGroups(groups="smoke")
	public void beforegroups() {
		System.out.println("the annotated method will run before first test method belongs to any groups ");
	}

	@Test
	public void tc_01() {
		System.out.println("Test 1");
	}
	@Test(groups = "smoke")
	public void Smoke(){
		System.out.println("smoke group ");
		
	}
	@Test(groups = "Regression")
	public void Regression(){
		System.out.println("Regresion group ");
		
	}
	public void tc_groups() {
		System.out.println("Test 1");
	}

	@Test
	public void tc_02() {
		System.out.println("Test 2");
	}

	@Test
	public void tc_03() {
		System.out.println("Test 3");
	}

	@AfterSuite
	public void aftersuite() {
		System.out.println("The annoted method will run after all test in this suite have run");
	}

	@AfterTest
	public void aftertest() {
		System.out.println("The annoted method after any test method beloging to classes inside any test tag is run");
	}

	@AfterClass
	public void afterclass() {
		System.out.println("The annotated method will be run after all test method in the current class is invoked");
	}

	@AfterMethod
	public void aftermethod() {

		System.out.println("The annoated method will be run after each test method");
	}
	@AfterGroups
	public void aftergroups() {
		System.out.println("the annotated method will run last test method belongs to any groups ");
	}

}
