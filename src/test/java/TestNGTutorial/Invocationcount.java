package TestNGTutorial;

import org.testng.annotations.Test;

public class Invocationcount {
	@Test(invocationCount = 10, threadPoolSize = 3, invocationTimeOut = 10)
	public void testRunMultipletimes() throws InterruptedException {

		System.out.println("Thread Id:" + Thread.currentThread().getId());
	}
}

