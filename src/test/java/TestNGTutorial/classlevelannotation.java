package TestNGTutorial;

import org.testng.annotations.Test;

@Test
public class classlevelannotation {
	public void test1() {
		System.out.println("test 1");
	}
	@Test(groups= {"dp"})
	public void test2() {
		System.out.println("test 2");
	}

}
