package TestNGTutorial;

import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

public class dependsOnGroups {
	@BeforeGroups(groups = "database")
	public void setupdatabase() {
		System.out.println("Dabase setup ");
		
	}
	@Test(groups="database")
	public void insert() {
		System.out.println("Insert data in a database");
	}
	@Test(groups="database")
	public void update() {
		System.out.println("Update data in a database");
	}
	@Test(groups="database")
	public void delete() {
		System.out.println("dalete data in a database");
	}
	@Test(groups="UI")
	public void accessUI() {
		System.out.println("UI access");
		Assert.fail();
	}
	@Test(dependsOnGroups = {"UI"})
	public void Login() {
		System.out.println("Login");
	}

}
