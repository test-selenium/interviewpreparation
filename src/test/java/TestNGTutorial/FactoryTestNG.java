package TestNGTutorial;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

public class FactoryTestNG {
	
	@Factory(dataProvider ="getdata")
	public Object[] Runalltestmethod(String s) {
		Object[] tests = new Object[2];
		tests[0] = new FactoryTest1(s);
		tests[1] = new FactoryTest2();
		return tests;
		
		
	}
	@DataProvider
	public Object[] getdata() {
		return new Object[] {"A", "B"};
	}

}
